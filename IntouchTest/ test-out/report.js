$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/features/Smoke/Cookie/CookiePopUp.feature");
formatter.feature({
  "line": 1,
  "name": "CookiePopUp",
  "description": "",
  "id": "cookiepopup",
  "keyword": "Feature"
});
formatter.before({
  "duration": 7412026844,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Check Cookie Popup appears",
  "description": "",
  "id": "cookiepopup;check-cookie-popup-appears",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I see the pop up appears",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I Click ok thanks",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "the pop up disappears",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2814045540,
  "status": "passed"
});
formatter.match({
  "location": "CookieStep.iSeeThePopUpAppears()"
});
formatter.result({
  "duration": 36632627,
  "status": "passed"
});
formatter.match({
  "location": "CookieStep.iClickOkThanks()"
});
formatter.result({
  "duration": 1654254452,
  "status": "passed"
});
formatter.match({
  "location": "CookieStep.thePopUpDisappears()"
});
formatter.result({
  "duration": 33887057,
  "status": "passed"
});
formatter.after({
  "duration": 48034380,
  "status": "passed"
});
formatter.before({
  "duration": 4200900112,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Check Find out more link",
  "description": "",
  "id": "cookiepopup;check-find-out-more-link",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "I see the pop up appears",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "I Click find out more link",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "I land to that page",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2523450444,
  "status": "passed"
});
formatter.match({
  "location": "CookieStep.iSeeThePopUpAppears()"
});
formatter.result({
  "duration": 34897176,
  "status": "passed"
});
formatter.match({
  "location": "CookieStep.iClickFindOutMoreLink()"
});
formatter.result({
  "duration": 6241960299,
  "status": "passed"
});
formatter.match({
  "location": "CookieStep.iLandToThatPage()"
});
formatter.result({
  "duration": 13462602,
  "status": "passed"
});
formatter.after({
  "duration": 2127659017,
  "status": "passed"
});
formatter.uri("src/test/resources/features/Smoke/FooterLinks/FooterLinks.feature");
formatter.feature({
  "line": 1,
  "name": "FooterLinks",
  "description": "",
  "id": "footerlinks",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "check Footer Links",
  "description": "",
  "id": "footerlinks;check-footer-links",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I login as Installer",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I click \u003cfooterlinks\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I see \u003cfooterlinks\u003e page",
  "keyword": "Then "
});
formatter.examples({
  "line": 10,
  "name": "",
  "description": "",
  "id": "footerlinks;check-footer-links;",
  "rows": [
    {
      "cells": [
        "footerlinks"
      ],
      "line": 11,
      "id": "footerlinks;check-footer-links;;1"
    },
    {
      "cells": [
        "Privacy Policy Notice"
      ],
      "line": 12,
      "id": "footerlinks;check-footer-links;;2"
    },
    {
      "cells": [
        "Terms of use"
      ],
      "line": 13,
      "id": "footerlinks;check-footer-links;;3"
    },
    {
      "cells": [
        "Cookie Policy"
      ],
      "line": 14,
      "id": "footerlinks;check-footer-links;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 4033009564,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "check Footer Links",
  "description": "",
  "id": "footerlinks;check-footer-links;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I login as Installer",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I click Privacy Policy Notice",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I see Privacy Policy Notice page",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2305355688,
  "status": "passed"
});
formatter.match({
  "location": "FooterLinksStep.iLoginAsInstaller()"
});
formatter.result({
  "duration": 3445710334,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Privacy Policy Notice",
      "offset": 8
    }
  ],
  "location": "FooterLinksStep.iClickFooterlinks(String)"
});
formatter.result({
  "duration": 2554312798,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Privacy Policy Notice",
      "offset": 6
    }
  ],
  "location": "FooterLinksStep.iSeeFooterlinksPage(String)"
});
formatter.result({
  "duration": 1065407916,
  "status": "passed"
});
formatter.after({
  "duration": 75269043,
  "status": "passed"
});
formatter.before({
  "duration": 3798220340,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "check Footer Links",
  "description": "",
  "id": "footerlinks;check-footer-links;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I login as Installer",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I click Terms of use",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I see Terms of use page",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2427713250,
  "status": "passed"
});
formatter.match({
  "location": "FooterLinksStep.iLoginAsInstaller()"
});
formatter.result({
  "duration": 3093013214,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Terms of use",
      "offset": 8
    }
  ],
  "location": "FooterLinksStep.iClickFooterlinks(String)"
});
formatter.result({
  "duration": 2544142723,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Terms of use",
      "offset": 6
    }
  ],
  "location": "FooterLinksStep.iSeeFooterlinksPage(String)"
});
formatter.result({
  "duration": 1089984398,
  "status": "passed"
});
formatter.after({
  "duration": 71184873,
  "status": "passed"
});
formatter.before({
  "duration": 3867270339,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "check Footer Links",
  "description": "",
  "id": "footerlinks;check-footer-links;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I login as Installer",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I click Cookie Policy",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I see Cookie Policy page",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2855229616,
  "status": "passed"
});
formatter.match({
  "location": "FooterLinksStep.iLoginAsInstaller()"
});
formatter.result({
  "duration": 2805058821,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Cookie Policy",
      "offset": 8
    }
  ],
  "location": "FooterLinksStep.iClickFooterlinks(String)"
});
formatter.result({
  "duration": 2551451050,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Cookie Policy",
      "offset": 6
    }
  ],
  "location": "FooterLinksStep.iSeeFooterlinksPage(String)"
});
formatter.result({
  "duration": 1096783811,
  "status": "passed"
});
formatter.after({
  "duration": 70793677,
  "status": "passed"
});
formatter.uri("src/test/resources/features/Smoke/Login/LoginUserRoles.feature");
formatter.feature({
  "line": 1,
  "name": "LoginUserRoles",
  "description": "",
  "id": "loginuserroles",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "check login",
  "description": "",
  "id": "loginuserroles;check-login",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter \u003cuser\u003e email and password",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I am logged in as that \u003cuser\u003e",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "loginuserroles;check-login;",
  "rows": [
    {
      "cells": [
        "user"
      ],
      "line": 10,
      "id": "loginuserroles;check-login;;1"
    },
    {
      "cells": [
        "Installer Without Training"
      ],
      "line": 11,
      "id": "loginuserroles;check-login;;2"
    },
    {
      "cells": [
        "Installer With Training"
      ],
      "line": 12,
      "id": "loginuserroles;check-login;;3"
    },
    {
      "cells": [
        "Company Admin Without Training"
      ],
      "line": 13,
      "id": "loginuserroles;check-login;;4"
    },
    {
      "cells": [
        "Company Admin With Training"
      ],
      "line": 14,
      "id": "loginuserroles;check-login;;5"
    },
    {
      "cells": [
        "Company Owner Without Training"
      ],
      "line": 15,
      "id": "loginuserroles;check-login;;6"
    },
    {
      "cells": [
        "Company Owner With Training"
      ],
      "line": 16,
      "id": "loginuserroles;check-login;;7"
    },
    {
      "cells": [
        "Admin"
      ],
      "line": 17,
      "id": "loginuserroles;check-login;;8"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3827076849,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "check login",
  "description": "",
  "id": "loginuserroles;check-login;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter Installer Without Training email and password",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I am logged in as that Installer Without Training",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2927297121,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Installer Without Training",
      "offset": 8
    }
  ],
  "location": "LoginUserRolesStep.iEnterUserEmailAndPassword(String)"
});
formatter.result({
  "duration": 3186835037,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Installer Without Training",
      "offset": 23
    }
  ],
  "location": "LoginUserRolesStep.iAmLoggedInAsThatUser(String)"
});
formatter.result({
  "duration": 28993248,
  "status": "passed"
});
formatter.after({
  "duration": 61215279,
  "status": "passed"
});
formatter.before({
  "duration": 3915847562,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "check login",
  "description": "",
  "id": "loginuserroles;check-login;;3",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter Installer With Training email and password",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I am logged in as that Installer With Training",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2583471057,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Installer With Training",
      "offset": 8
    }
  ],
  "location": "LoginUserRolesStep.iEnterUserEmailAndPassword(String)"
});
formatter.result({
  "duration": 2728443979,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Installer With Training",
      "offset": 23
    }
  ],
  "location": "LoginUserRolesStep.iAmLoggedInAsThatUser(String)"
});
formatter.result({
  "duration": 28889409,
  "status": "passed"
});
formatter.after({
  "duration": 44296682,
  "status": "passed"
});
formatter.before({
  "duration": 3822841031,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "check login",
  "description": "",
  "id": "loginuserroles;check-login;;4",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter Company Admin Without Training email and password",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I am logged in as that Company Admin Without Training",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2468311816,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Company Admin Without Training",
      "offset": 8
    }
  ],
  "location": "LoginUserRolesStep.iEnterUserEmailAndPassword(String)"
});
formatter.result({
  "duration": 3478145185,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Company Admin Without Training",
      "offset": 23
    }
  ],
  "location": "LoginUserRolesStep.iAmLoggedInAsThatUser(String)"
});
formatter.result({
  "duration": 54522275,
  "status": "passed"
});
formatter.after({
  "duration": 58911795,
  "status": "passed"
});
formatter.before({
  "duration": 3866701280,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "check login",
  "description": "",
  "id": "loginuserroles;check-login;;5",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter Company Admin With Training email and password",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I am logged in as that Company Admin With Training",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2523342492,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Company Admin With Training",
      "offset": 8
    }
  ],
  "location": "LoginUserRolesStep.iEnterUserEmailAndPassword(String)"
});
formatter.result({
  "duration": 3551599098,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Company Admin With Training",
      "offset": 23
    }
  ],
  "location": "LoginUserRolesStep.iAmLoggedInAsThatUser(String)"
});
formatter.result({
  "duration": 40067547,
  "status": "passed"
});
formatter.after({
  "duration": 53870453,
  "status": "passed"
});
formatter.before({
  "duration": 3836414154,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "check login",
  "description": "",
  "id": "loginuserroles;check-login;;6",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter Company Owner Without Training email and password",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I am logged in as that Company Owner Without Training",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2301134263,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Company Owner Without Training",
      "offset": 8
    }
  ],
  "location": "LoginUserRolesStep.iEnterUserEmailAndPassword(String)"
});
formatter.result({
  "duration": 3015886887,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Company Owner Without Training",
      "offset": 23
    }
  ],
  "location": "LoginUserRolesStep.iAmLoggedInAsThatUser(String)"
});
formatter.result({
  "duration": 28707947,
  "status": "passed"
});
formatter.after({
  "duration": 54808090,
  "status": "passed"
});
formatter.before({
  "duration": 3835331039,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "check login",
  "description": "",
  "id": "loginuserroles;check-login;;7",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter Company Owner With Training email and password",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I am logged in as that Company Owner With Training",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2373528195,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Company Owner With Training",
      "offset": 8
    }
  ],
  "location": "LoginUserRolesStep.iEnterUserEmailAndPassword(String)"
});
formatter.result({
  "duration": 3496231715,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Company Owner With Training",
      "offset": 23
    }
  ],
  "location": "LoginUserRolesStep.iAmLoggedInAsThatUser(String)"
});
formatter.result({
  "duration": 28202631,
  "status": "passed"
});
formatter.after({
  "duration": 52644944,
  "status": "passed"
});
formatter.before({
  "duration": 3955818981,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "check login",
  "description": "",
  "id": "loginuserroles;check-login;;8",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to qaintouch",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I enter Admin email and password",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I am logged in as that Admin",
  "matchedColumns": [
    0
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "LoginUserRolesStep.iNavigateToQaintouch()"
});
formatter.result({
  "duration": 2526427340,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Admin",
      "offset": 8
    }
  ],
  "location": "LoginUserRolesStep.iEnterUserEmailAndPassword(String)"
});
formatter.result({
  "duration": 5037007374,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Admin",
      "offset": 23
    }
  ],
  "location": "LoginUserRolesStep.iAmLoggedInAsThatUser(String)"
});
formatter.result({
  "duration": 28086968,
  "status": "passed"
});
formatter.after({
  "duration": 48391648,
  "status": "passed"
});
});