Feature: LoginUserRoles

  @smoke
  Scenario Outline: check login
    Given I navigate to qaintouch
    When I enter <user> email and password
    Then I am logged in as that <user>

    Examples:
      | user                           |
      | Installer Without Training     |
      | Installer With Training        |
      | Company Admin Without Training |
      | Company Admin With Training    |
      | Company Owner Without Training |
      | Company Owner With Training    |
      | Admin                          |

