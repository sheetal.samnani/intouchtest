Feature: FooterLinks

  @smoke
  Scenario Outline: check Footer Links
    Given I navigate to qaintouch
    And I login as Installer
    When I click <footerlinks>
    Then I see <footerlinks> page

    Examples:
      | footerlinks |
      | Privacy Policy Notice |
      | Terms of use |
      | Cookie Policy |
