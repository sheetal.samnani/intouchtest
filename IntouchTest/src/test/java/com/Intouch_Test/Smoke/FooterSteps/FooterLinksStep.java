package com.Intouch_Test.Smoke.FooterSteps;

import com.Intouch_Test.Utils.DriverManager;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FooterLinksStep extends DriverManager {
    com.Intouch_Test.Smoke.FooterLinks.FooterLinks Footer = new com.Intouch_Test.Smoke.FooterLinks.FooterLinks();

    @And("^I login as Installer$")
    public void iLoginAsInstaller()
    {
        Footer.LoginInstaller();
    }

    @When("^I click ([^\"]*)$")
    public void iClickFooterlinks(String link)
    {
        Footer.ClickFooterLinks(link);
    }

    @Then("^I see ([^\"]*) page$")
    public void iSeeFooterlinksPage(String link)
    {
        Footer.VerifyFooterLink(link);
    }
}
