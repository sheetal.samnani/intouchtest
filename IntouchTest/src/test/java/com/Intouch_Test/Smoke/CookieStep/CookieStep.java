package com.Intouch_Test.Smoke.CookieStep;

import com.Intouch_Test.Utils.DriverManager;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.Intouch_Test.Smoke.Cookie.CookiePopUp;
import com.Intouch_Test.Utils.Utils;

public class CookieStep extends DriverManager {
    CookiePopUp cookiepopup =  new CookiePopUp();

    @Then("^I see the pop up appears$")
    public void iSeeThePopUpAppears() {
        cookiepopup.CheckPopUpAppears();
    }

    @When("^I Click ok thanks$")
    public void iClickOkThanks() {cookiepopup.ClickAgreePopUp();
    }

    @Then("^the pop up disappears$")
    public void thePopUpDisappears() {cookiepopup.CheckPopUpDisappear();
    }

    @When("^I Click find out more link$")
    public void iClickFindOutMoreLink() { cookiepopup.ClickFindOutMoreLink();
    }

    @Then("^I land to that page$")
    public void iLandToThatPage() {cookiepopup.CheckUrlCookiePolicy();
    }
}
