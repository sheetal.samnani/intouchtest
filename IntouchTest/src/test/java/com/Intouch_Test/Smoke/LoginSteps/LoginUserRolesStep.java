package com.Intouch_Test.Smoke.LoginSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import com.Intouch_Test.Utils.DriverManager;

import java.io.IOException;

public class LoginUserRolesStep extends DriverManager
{
    com.Intouch_Test.Login.LoginUserRoles login = new com.Intouch_Test.Login.LoginUserRoles();

    @Given("^I navigate to qaintouch$")
    public void iNavigateToQaintouch() throws IOException {

        login.NavigateFunction();
    }

    @When("^I enter ([^\"]*) email and password$")
    public void iEnterUserEmailAndPassword(String tab) { login.Login(tab);
    }

    @Then("^I am logged in as that ([^\"]*)$")
    public void iAmLoggedInAsThatUser(String tab) {login.VerifyLogin(tab);
    }
}
