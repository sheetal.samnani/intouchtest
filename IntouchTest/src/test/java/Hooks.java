import cucumber.api.java.Before;
import cucumber.api.java.After;
import com.Intouch_Test.Utils.DriverManager;

import java.io.IOException;

public class Hooks
{
    @Before
    public void SetUp() throws IOException
    {
        new DriverManager().getProperties();
        new DriverManager().OpenBrowser();
    }

    @After
    public void TearDown()
    {
        new DriverManager().CloseBrowser();
    }
}
