import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = ".",
        format = {"pretty", "html: test-out", "json: json_output/cucumber.json","junit:junit_xml/cucumber.xml"},
        tags = {"@smoke"},
        dryRun = false)
public class Runner {
}
