package com.Intouch_Test.Utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


import java.util.concurrent.TimeUnit;

public class DriverManager
{
    public static WebDriver driver;
    static Properties properties;
    private static String browser;
    private static String url;


    public void getProperties()throws IOException {

        properties = new Properties();
        FileInputStream fis = new FileInputStream(Paths.get(System.getProperty("user.dir"), "src", "test", "resources", "config.properties").toString());
        //FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\resources\\config.properties");
        properties.load(fis);

        browser= properties.getProperty("browser");
        //url= properties.getProperty("url");
        url=properties.getProperty("url");

    }


    public void OpenBrowser()
    {

        if (browser.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", "geckodriver");
            driver = new FirefoxDriver();
        } else if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
            driver = new ChromeDriver();
        }
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
    }

   // public void OpenBrowser1()
  //  {
   //     System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        //System.setProperty("webdriver.chrome.driver","C:/Users/ukssam/IdeaProjects/IntouchTest/chromedriver.exe");
    //    driver = new ChromeDriver();
  //      driver.get("https://qa.intouch.bmigroup.com/user/login");
  //      driver.manage().window().maximize();
    //    driver.manage().deleteAllCookies();
  //      driver.manage().timeouts().implicitlyWait(45, TimeUnit.SECONDS);
  //  }
    public void CloseBrowser()
    {
        driver.close();
    }
}
