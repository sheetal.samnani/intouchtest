package com.Intouch_Test.Smoke.Cookie;

import com.Intouch_Test.Utils.DriverManager;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.Intouch_Test.Utils.Utils;

import java.sql.Driver;
import java.util.ArrayList;

public class CookiePopUp extends DriverManager
{
    public CookiePopUp()
    {
        PageFactory.initElements(driver,this);
    }
    @FindBy(how = How.ID,using = "popup-text")
    private WebElement PopUpComplianceMessage;

    @FindBy(how = How.CSS,using = ".agree-button.eu-cookie-compliance-secondary-button")
    private WebElement AgreeButton;

    @FindBy(how = How.CSS, using = ".find-more-button-processed")
    private WebElement FindOutMoreLink;

    public void CheckPopUpAppears()
    {
        assert PopUpComplianceMessage.isDisplayed();
    }

    public void ClickAgreePopUp()
    {
       Utils.waitForSomeTime();
        AgreeButton.click();
        Utils.waitForSomeTime();
    }
    public boolean CheckPopUpDisappear()
    {
        if (PopUpComplianceMessage.isDisplayed())
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public void ClickFindOutMoreLink()
    {
        Utils.waitForSomeTime();
        FindOutMoreLink.click();
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        url = driver.getCurrentUrl();
        driver.close();

    }

    public void CheckUrlCookiePolicy()
    {
        assert url.contains(FindOutMoreUrl);
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(0));
    }
    private String FindOutMoreUrl = "https://www.bmigroup.com/legal/third-party-privacy-notice";
    private String url;


}
