package com.Intouch_Test.Smoke.FooterLinks;

import com.Intouch_Test.Utils.DriverManager;
import com.Intouch_Test.Utils.Utils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.net.UrlChecker;
import org.openqa.selenium.net.Urls;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.interactions.Actions;
import javax.print.DocFlavor;
import java.util.ArrayList;
import java.util.List;

public class FooterLinks extends DriverManager
{
    public FooterLinks()
    {
        PageFactory.initElements(driver,this);
    }
    @FindBy(how = How.CSS,using = ".logged-in__footer > div > div > div > div > ul > li")
    private List<WebElement> FooterLinks;

    @FindBy(how = How.ID,using = "edit-name")
    public WebElement UserName;

    @FindBy(how = How.ID,using = "edit-pass")
    private WebElement Password;

    @FindBy(how = How.ID,using = "edit-submit")
    private WebElement SubmitButton;

    @FindBy(how = How.CSS,using = ".eu-cookie-compliance-secondary-button")
    private WebElement AgreeButton;

    private String PrivacyUrl;
    private String TermsUrl;
    private String CookieUrl;

    private String PrivacyPolicyUrl = "https://qa.intouch.bmigroup.com/en/general/privacypolicynotice";
    private String TermsOfUseUrl = "https://qa.intouch.bmigroup.com/en/general/termsofuse";
    private String CookiePolicyUrl = "https://qa.intouch.bmigroup.com/en/cookiepolicy";

    public void LoginInstaller()
    {
        UserName.sendKeys("sheetal.samnani+555@bmigroup.com");
        Password.sendKeys("password");
        SubmitButton.click();
        Utils.waitForSomeTime();
    }

    public void ClickFooterLinks(String link)
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, Math.max(document.documentElement.scrollHeight, document.body.scrollHeight, document.documentElement.clientHeight));");
        Utils.waitForSomeTime();
        AgreeButton.click();
        Utils.waitForSomeTime();

        switch (link)
        {
            case "Privacy Policy Notice":
                FooterLinks.get(0).click();
                Utils.waitForSomeTime();
                break;
            case "Terms of use":
                FooterLinks.get(1).click();
                Utils.waitForSomeTime();
                break;
            case "Cookie Policy":
                FooterLinks.get(2).click();
                Utils.waitForSomeTime();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + link);
        }
        //FooterLinks = new ArrayList<WebElement>();
        //for (WebElement item : FooterLinks)
        //{
        //  item.click();
        //Utils.waitForSomeTime();
    }

    public void VerifyFooterLink(String link)
    {
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        switch (link)
        {
            case "Privacy Policy Notice":
                PrivacyUrl = driver.getCurrentUrl();
                assert PrivacyUrl.contains(PrivacyPolicyUrl);
                driver.close();
                ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
                driver.switchTo().window(tabs.get(0));
                break;
            case "Terms of use":
                TermsUrl = driver.getCurrentUrl();
                assert TermsUrl.contains(TermsOfUseUrl);
                driver.close();
                ArrayList<String> tab = new ArrayList<String> (driver.getWindowHandles());
                driver.switchTo().window(tab.get(0));
                break;
            case "Cookie Policy":
                CookieUrl = driver.getCurrentUrl();
                assert CookieUrl.contains(CookiePolicyUrl);
                driver.close();
                ArrayList<String> tab1 = new ArrayList<String> (driver.getWindowHandles());
                driver.switchTo().window(tab1.get(0));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + link);
        }
        //ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        //driver.switchTo().window(tabs2.get(0));
        //driver.close();
    }
}
