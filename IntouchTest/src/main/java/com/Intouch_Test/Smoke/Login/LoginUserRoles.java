package com.Intouch_Test.Login;

import com.Intouch_Test.Utils.DriverManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.Intouch_Test.Utils.Utils;

public class LoginUserRoles extends DriverManager
{
    public LoginUserRoles()
    {
        PageFactory.initElements(driver,this);
    }

    @FindBy(how = How.ID,using = "edit-name")
    public WebElement UserName;

    @FindBy(how = How.ID,using = "edit-pass")
    private WebElement Password;

    @FindBy(how = How.ID,using = "edit-submit")
    private WebElement SubmitButton;

    @FindBy(how = How.CLASS_NAME,using = "contractor-header__title")
    private WebElement HeaderTitle;

    @FindBy(how = How.ID,using = "toolbar-administration")
    private WebElement AdminHeader;
    public void NavigateFunction()
    {
        driver.navigate().to("https://qa.intouch.bmigroup.com/user/login");
        Utils.waitForSomeTime();
    }
    public void Login(String tab)
        {
            switch(tab.toLowerCase())
            {
                case "installer without training" :
                    UserName.sendKeys("sheetal.samnani+555@bmigroup.com");
                    Password.sendKeys("password");
                    SubmitButton.click();
                    Utils.waitForSomeTime();
                    break;
                case "installer with training":
                    UserName.sendKeys("sheetal.samnani+5555@bmigroup.com");
                    Password.sendKeys("password");
                    SubmitButton.click();
                    Utils.waitForSomeTime();
                    break;
                case "company admin without training":
                    UserName.sendKeys("sheetal.samnani+666@bmigroup.com");
                    Password.sendKeys("password");
                    SubmitButton.click();
                    Utils.waitForSomeTime();
                    break;
                case "company admin with training":
                    UserName.sendKeys("sheetal.samnani+6666@bmigroup.com");
                    Password.sendKeys("password");
                    SubmitButton.click();
                    Utils.waitForSomeTime();
                    break;
                case "company owner without training":
                    UserName.sendKeys("sheetal.samnani+777@bmigroup.com");
                    Password.sendKeys("password");
                    SubmitButton.click();
                    Utils.waitForSomeTime();
                    break;
                case "company owner with training":
                    UserName.sendKeys("sheetal.samnani+7777@bmigroup.com");
                    Password.sendKeys("password");
                    SubmitButton.click();
                    Utils.waitForSomeTime();
                    break;
                case "admin":
                    UserName.sendKeys("admin");
                    Password.sendKeys("admin");
                    SubmitButton.click();
                    Utils.waitForSomeTime();

                case "default":
                    System.out.println("Wrong user entered");
                    break;
            }

        }

    public void VerifyLogin(String tab)
    {
        switch(tab.toLowerCase())
        {
            case "installer without training" :
                assert HeaderTitle.isDisplayed();
                break;
            case "installer with training":
                assert HeaderTitle.isDisplayed();
                break;
            case "company admin without training":
                assert HeaderTitle.isDisplayed();
                break;
            case "company admin with training":
                assert HeaderTitle.isDisplayed();
                break;
            case "company owner without training":
                assert HeaderTitle.isDisplayed();
                break;
            case "company owner with training":
                assert HeaderTitle.isDisplayed();
                break;
            case "admin":
                assert AdminHeader.isDisplayed();
            case "default":
                System.out.println("Wrong user entered");
                break;
        }
    }

}
